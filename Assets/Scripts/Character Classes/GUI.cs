﻿using UnityEngine;
using System.Collections;

public class GUI : MonoBehaviour {

    private BaseCharacterClass class1 = new BaseWarriorClass();
    private BaseCharacterClass class2 = new BaseMageClass();


	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI () {

        // Warrior
        GUILayout.Label(class1.CharacterClassName);
        GUILayout.Label(class1.CharacterClassDescription);
        GUILayout.Label(class1.Strenght.ToString());                   //number values, not strings

        // Mage
        GUILayout.Label(class2.CharacterClassName);
        GUILayout.Label(class2.CharacterClassDescription);
    }
}
