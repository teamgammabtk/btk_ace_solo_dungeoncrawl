﻿using UnityEngine;
using System.Collections;

public class BaseEquipment : BaseStatItem
{

    public enum EquipmentTypes
    {
        HEAD,
        CHEST,
        SHOULDERS,
        ARM,
        LEGS,
        NECK,
        FEET,
        EARRING,
        RING,
        RING2

    }

    private EquipmentTypes equipmentType;

    public EquipmentTypes EquipmentType {
        get { return equipmentType; }
        set { equipmentType = value; }
    }

    public int SpellEffectID  {
        get { return SpellEffectID; }
        set { SpellEffectID = value;}
    }
}