﻿using UnityEngine;
using System.Collections;

public class BaseCharacterClass {

    private string characterClassName;
    private string characterClassDescription;

    //Stats
    private int stamina;
    private int endurance;
    private int strenght;
    private int intellect;
    private int initiative;

    public string CharacterClassName {

        get {return characterClassName; }
        set {characterClassName = value;}

    }

    public string CharacterClassDescription {

        get { return characterClassDescription; }
        set { characterClassDescription = value; }

    }

    //Statline Set,ers and Get,ers

    public int Stamina

    {
        get { return stamina; }
        set { stamina = value; }
    }

    public int Endurance

    {
        get { return endurance; }
        set { endurance = value; }
    }

    public int Strenght

    {
        get { return strenght; }
        set { strenght = value; }
    }

    public int Intellect

    {
        get { return intellect; }
        set { intellect = value; }
    }

    public int Initiative

    {
        get { return initiative; }
        set { initiative = value; }
    }


}